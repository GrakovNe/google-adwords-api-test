package org.grakovne.google.adwords;// Copyright 2013 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import com.google.api.ads.common.lib.auth.GoogleClientSecretsBuilder;
import com.google.api.ads.common.lib.auth.GoogleClientSecretsBuilder.Api;
import com.google.api.ads.common.lib.exception.ValidationException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

public class GetRefreshToken {

    public static final String ADWORDS_API_SCOPE = "https://www.googleapis.com/auth/adwords";
    private static final List<String> SCOPES = Collections.singletonList(ADWORDS_API_SCOPE);
    private static final String CALLBACK_URL = "urn:ietf:wg:oauth:2.0:oob";

    private static Credential getOAuth2Credential(GoogleClientSecrets clientSecrets)
        throws IOException {
        GoogleAuthorizationCodeFlow authorizationFlow = new GoogleAuthorizationCodeFlow.Builder(
            new NetHttpTransport(),
            new JacksonFactory(),
            clientSecrets,
            SCOPES)
            .setAccessType("offline").build();

        String authorizeUrl = authorizationFlow.newAuthorizationUrl().setRedirectUri(CALLBACK_URL).build();
        System.out.printf("Paste this url in your browser:%n%s%n", authorizeUrl);
        System.out.println("Type the code you received here: ");
        String authorizationCode = new BufferedReader(new InputStreamReader(System.in)).readLine();

        GoogleAuthorizationCodeTokenRequest tokenRequest = authorizationFlow.newTokenRequest(authorizationCode);
        tokenRequest.setRedirectUri(CALLBACK_URL);
        GoogleTokenResponse tokenResponse = tokenRequest.execute();

        GoogleCredential credential = new GoogleCredential.Builder()
            .setTransport(new NetHttpTransport())
            .setJsonFactory(new JacksonFactory())
            .setClientSecrets(clientSecrets)
            .build();

        return credential.setFromTokenResponse(tokenResponse);
    }

    public static void main(String[] args) {

        GoogleClientSecrets clientSecrets = null;
        try {
            clientSecrets = new GoogleClientSecretsBuilder()
                .forApi(Api.ADWORDS)
                .withClientSecrets("874998454555-7edhdt5avskfm86rpf5pgreg262eg0th.apps.googleusercontent.com", "HLGJhr3NKpOReOykfEfP-6lO")
                .build();
        } catch (ValidationException e) {
            System.exit(-1);
            return;
        }

        try {
            Credential credential = getOAuth2Credential(clientSecrets);
            System.out.printf("Your refresh token is: %s%n", credential.getRefreshToken());
        } catch (IOException ioe) {
            System.err.printf("Failed to generate credentials. Exception: %s%n", ioe);
        }
    }
}