package org.grakovne.google.adwords.examples;

import com.google.api.ads.adwords.axis.factory.AdWordsServices;
import com.google.api.ads.adwords.axis.utils.v201710.SelectorBuilder;
import com.google.api.ads.adwords.axis.v201710.cm.Campaign;
import com.google.api.ads.adwords.axis.v201710.cm.CampaignPage;
import com.google.api.ads.adwords.axis.v201710.cm.CampaignServiceInterface;
import com.google.api.ads.adwords.axis.v201710.cm.Selector;
import com.google.api.ads.adwords.lib.client.AdWordsSession;
import com.google.api.ads.adwords.lib.selectorfields.v201710.cm.CampaignField;
import com.google.api.ads.common.lib.exception.OAuthException;
import org.grakovne.google.adwords.utils.AuthHelper;

import java.rmi.RemoteException;

public class GetCampaignsExample {

    public static void main(String[] args) throws OAuthException, RemoteException {
        CampaignServiceInterface campaignService =
            AdWordsServices.getInstance().get(AuthHelper.getSession(), CampaignServiceInterface.class);

        int offset = 0;

        SelectorBuilder builder = new SelectorBuilder();
        Selector selector = builder
            .fields(CampaignField.Id, CampaignField.Name)
            .orderAscBy(CampaignField.Name)
            .offset(offset)
            .limit(100)
            .build();

        CampaignPage page;
        do {
            page = campaignService.get(selector);
            if (page.getEntries() != null) {
                for (Campaign campaign : page.getEntries()) {
                    System.out.printf("Campaign with name '%s' and ID %d was found.%n", campaign.getName(),
                        campaign.getId());
                }
            } else {
                System.out.println("No campaigns were found.");
            }

            offset += 100;
            selector = builder.increaseOffsetBy(100).build();
        } while (offset < page.getTotalNumEntries());
    }
}
