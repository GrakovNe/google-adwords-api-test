package org.grakovne.google.adwords.utils;

import com.google.api.ads.adwords.lib.client.AdWordsSession;
import com.google.api.ads.common.lib.auth.OfflineCredentials;
import com.google.api.ads.common.lib.exception.OAuthException;
import com.google.api.ads.common.lib.exception.ValidationException;
import com.google.api.client.auth.oauth2.Credential;

public class AuthHelper {

    public static AdWordsSession getSession() throws OAuthException {
        try {
            Credential oAuth2Credential =
                new OfflineCredentials.Builder()
                    .forApi(OfflineCredentials.Api.ADWORDS)
                    .withRefreshToken("1/Ap6aCJRKePAcWKijKqMkT8f5GsTF68ImYFokVU4_38U")
                    .withClientSecrets("874998454555-7edhdt5avskfm86rpf5pgreg262eg0th.apps.googleusercontent.com",
                        "HLGJhr3NKpOReOykfEfP-6lO")
                    .build()
                    .generateCredential();

            return new AdWordsSession.Builder()
                .withClientCustomerId("709-030-9742")
                .withDeveloperToken("5R9IbpFe_Hq-FBu7e_E8rQ")
                .withOAuth2Credential(oAuth2Credential).build();

        } catch (ValidationException e) {
            throw new OAuthException("can't parse credentials");
        }
    }

}